# Databricks notebook source
# MAGIC %md
# MAGIC ### Trying to use and query the patient feature store

# COMMAND ----------

# MAGIC %sql 
# MAGIC 
# MAGIC select *
# MAGIC from patient_store.patient_demographics version as of 9;
# MAGIC 
# MAGIC -- SELECT * FROM default.people10m VERSION AS OF 0
# MAGIC 
# MAGIC -- select * from patient_store.patient_demographics@20220309191042000
# MAGIC -- select * from patient_store.patient_demographics@20220309225600000
# MAGIC -- SELECT * FROM patient_store.patient_demographics TIMESTAMP AS OF date_sub( current_date(), 0 )

# COMMAND ----------

# MAGIC %sql -- Join in data outside of feature store
# MAGIC select
# MAGIC   pd.*,
# MAGIC   ct.hypertension,
# MAGIC   ct.alcoholism
# MAGIC from
# MAGIC   patient_store.patient_demographics pd
# MAGIC   inner join condition_t ct on ct.patientid = pd.patientid;
