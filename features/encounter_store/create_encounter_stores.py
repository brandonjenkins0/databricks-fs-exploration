# Databricks notebook source
from databricks import feature_store

# Loading in the patient table
encounter_raw_df = spark.sql('select patientid, appointmentid from encounter_t;')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating patient feature store database
# MAGIC create database if not exists encounter_store;

# COMMAND ----------

# Instantiating the Feature Store Client
fs = feature_store.FeatureStoreClient()

# Adding patient base table to the feature store
fs.create_table(
    name='encounter_store.encounter_dev',  # database.tablename
    primary_keys=['patientid', 'appointmentid'],
    df=encounter_raw_df,
    description='encounter dev'
)

# COMMAND ----------

# Instantiating the Feature Store Client
fs = feature_store.FeatureStoreClient()

# Adding patient base table to the feature store
fs.create_table(
    name='encounter_store.encounter_test',  # database.tablename
    primary_keys=['patientid', 'appointmentid'],
    df=encounter_raw_df,
    description='encounter test'
)

# COMMAND ----------

# Instantiating the Feature Store Client
fs = feature_store.FeatureStoreClient()

# Adding patient base table to the feature store
fs.create_table(
    name='encounter_store.encounter_prod',  # database.tablename
    primary_keys=['patientid', 'appointmentid'],
    df=encounter_raw_df,
    description='encounter prod'
)

# COMMAND ----------


