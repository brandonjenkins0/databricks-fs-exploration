# Databricks notebook source
from databricks import feature_store

# COMMAND ----------

# Loading in the patient table
patient_raw_df = spark.sql('select patientid from patient_t;')

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Creating patient feature store database
# MAGIC create database if not exists patient_store;

# COMMAND ----------

# Metadata Template
long_name = 'Patient Demographics'
table_level = 'Production'
summary = 'The features in this patient demographics table have been tested, validated, and are continuously monitored for feature drift.'
monitor_dsc = 'To see a live monitoring dashboard of your features please visit'
monitor_name = long_name.lower().replace(' ', '-')
monitor_url = 'https://www.google.com'
data_lineage_dsc = 'Enter the entire data lineage for each feature'
feat_coverage_dsc = 'Enter the amount of coverage for each feature and the different types of tests'
contact_dsc = 'For further information or questions reach out to'
contact_email = 'no.one.imail.org'
other_meta_dsc = 'Cool stuff here'

feature_table_dsc = f"""
# {long_name} - {table_level}

{summary}

### **Feature Monitoring**
{monitor_dsc} [{monitor_name}](monitor_url)

### **Data Lineage**
{data_lineage_dsc}

### **Feature Coverage**
{feat_coverage_dsc}

### **Contact**
{contact_dsc} {contact_email}

### **Other Cool Metadata Categories**
{other_meta_dsc}
"""

# COMMAND ----------

# Instantiating the Feature Store Client
fs = feature_store.FeatureStoreClient()

# COMMAND ----------

# Adding patient base table to the feature store
fs.create_table(
    name='patient_store.patient_demographics',  # database.tablename
    primary_keys=['patientid'],
    df=patient_raw_df,
    description=feature_table_dsc
)

# COMMAND ----------


