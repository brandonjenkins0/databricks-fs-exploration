# Databricks notebook source
from databricks import feature_store

# COMMAND ----------

# MAGIC %sql
# MAGIC select
# MAGIC   patientid,
# MAGIC   age
# MAGIC from
# MAGIC   patient_t;

# COMMAND ----------

# Sql query to pull the data and transform
ft_age_sql = """
select patientid
  , age
from patient_t;
"""

# Bringing in the data
ft_age = spark.sql(ft_age_sql).toPandas()

# COMMAND ----------

ft_age['older_than_50'] = (ft_age['age'] > 50).astype(int)
ft_age.head()

# COMMAND ----------

ft_age_spark = spark.createDataFrame(ft_age)
ft_age_spark.head(10)

# COMMAND ----------

# Instantiating the feature store client
fs = feature_store.FeatureStoreClient()

# Uploading new feature to the patient demographics feature table
fs.write_table(
    name='patient_store.patient_demographics',
    df=ft_age_spark,
    mode='merge'
)

# COMMAND ----------



# COMMAND ----------


