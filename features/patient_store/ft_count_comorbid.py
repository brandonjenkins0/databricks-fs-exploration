# Databricks notebook source
# MAGIC %r
# MAGIC library(sparklyr)
# MAGIC library(dplyr)
# MAGIC sc <- spark_connect(method = "databricks")

# COMMAND ----------

# MAGIC %r
# MAGIC src_tbls(sc)

# COMMAND ----------

# MAGIC %r
# MAGIC condition_t = tbl(sc,"condition_t")
# MAGIC condition_t

# COMMAND ----------

# MAGIC %r
# MAGIC # Sql query to pull the data and transform
# MAGIC condition_t_new = condition_t %>%
# MAGIC   mutate(comorbid_count =  hypertension + diabetes + disability + alcoholism) %>%
# MAGIC   select(patientid, comorbid_count) %>% collect() %>%
# MAGIC   copy_to(sc, ., "condition_t_new")

# COMMAND ----------

# Instantiating the feature store client
from databricks import feature_store
fs = feature_store.FeatureStoreClient()

# Sql query to pull the data and transform
ft_count_comorbid_sql = """
select patientid
  , comorbid_count
from condition_t_new;
"""

# Bringing in the data
ft_count_comorbid = spark.sql(ft_count_comorbid_sql)

# Uploading new feature to the patient demographics feature table
fs.write_table(
    name='patient_store.patient_demographics',
    df=ft_count_comorbid,
    mode='merge'
)

# COMMAND ----------

spark.sql('drop table condition_t_new')
