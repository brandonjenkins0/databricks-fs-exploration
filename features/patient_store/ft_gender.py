# Databricks notebook source
# MAGIC %md
# MAGIC ### Create and add the gender feature to patient_store.patient_demographics

# COMMAND ----------

from databricks import feature_store

# COMMAND ----------

# Sql query to pull the data and transform
ft_gender_sql = """
select patientid
    , case when gender = 'M' then 0
           when gender = 'F' then 1
           else 2 end as gender 
from patient_t;
"""

# Bringing in the data
ft_gender = spark.sql(ft_gender_sql)

# COMMAND ----------

# Instantiating the feature store client
fs = feature_store.FeatureStoreClient()

# Uploading new feature to the patient demographics feature table
fs.write_table(
    name='patient_store.patient_demographics',
    df=ft_gender,
    mode='merge'
)
