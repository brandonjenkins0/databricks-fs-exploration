# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## Create tables via notebook
# MAGIC 
# MAGIC This notebook will show you how to create and query a table or DataFrame that you uploaded to DBFS. [DBFS](https://docs.databricks.com/user-guide/dbfs-databricks-file-system.html) is a Databricks File System that allows you to store data for querying inside of Databricks. This notebook assumes that you have a file already inside of DBFS that you would like to read from.
# MAGIC 
# MAGIC This notebook is written in **Python** so the default cell type is Python. However, you can use different languages by using the `%LANGUAGE` syntax. Python, Scala, SQL, and R are all supported.

# COMMAND ----------

# MAGIC %md
# MAGIC ### Patient Table

# COMMAND ----------

# File location and type
file_location = "/FileStore/tables/interim/patient.parquet"
file_type = "parquet"

# Loading table into spark dataframe
df = spark.read.format(file_type) \
  .load(file_location) \
  .select('patientid', 'gender', 'age')

display(df)

# COMMAND ----------

# Create a view or table
temp_table_name = "patient_v"
df.createOrReplaceTempView(temp_table_name)

# COMMAND ----------

# With this registered as a temp view, it will only be available to this particular notebook. If you'd like other users to be able to query this table, you can also create a table from the DataFrame.
# Once saved, this table will persist across cluster restarts as well as allow various users across different notebooks to query this data.
# To do so, choose your table name and uncomment the bottom line.

permanent_table_name = "patient_t"
spark.sql(f'drop table if exists {permanent_table_name}')
df.write.format("parquet").saveAsTable(permanent_table_name)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Condition Table

# COMMAND ----------

# File location and type
file_location = "/FileStore/tables/interim/condition.parquet"
file_type = "parquet"

# Loading table into spark dataframe
df = spark.read.format(file_type) \
  .load(file_location) \
  .select('patientid', 'medicaidind', 'hypertension', 'diabetes', 'alcoholism', 'disability')

display(df)

# Create a view or table
temp_table_name = "condition_v"
df.createOrReplaceTempView(temp_table_name)

# Creating permanent table that can be access acrossed notebooks and compute
permanent_table_name = "condition_t"
spark.sql(f'drop table if exists {permanent_table_name}')
df.write.format("parquet").saveAsTable(permanent_table_name)

# COMMAND ----------

# %sql
# drop table if exists condition_t

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Encounter Table

# COMMAND ----------

# File location and type
file_location = "/FileStore/tables/interim/encounter.parquet"
file_type = "parquet"

# Loading table into spark dataframe
df = spark.read.format(file_type) \
  .load(file_location) \
  .select('patientid', 'appointmentid', 'scheduledday', 'appointmentday', 'sms_received')

display(df)

# Create a view or table
temp_table_name = "encounter_v"
df.createOrReplaceTempView(temp_table_name)

# Creating permanent table that can be access acrossed notebooks and compute
permanent_table_name = "encounter_t"
spark.sql(f'drop table if exists {permanent_table_name}')
df.write.format("parquet").saveAsTable(permanent_table_name)

# COMMAND ----------

# MAGIC %md
# MAGIC ### No Show: target

# COMMAND ----------

# File location and type
file_location = "/FileStore/tables/interim/noshow.parquet"
file_type = "parquet"

# Loading table into spark dataframe
df = spark.read.format(file_type) \
  .load(file_location) \
  .select('patientid', 'appointmentid', 'no-show')

display(df)

# Create a view or table
temp_table_name = "noshow_v"
df.createOrReplaceTempView(temp_table_name)

# Creating permanent table that can be access acrossed notebooks and compute
permanent_table_name = "noshow_t"
spark.sql(f'drop table if exists {permanent_table_name}')  # Recreates the table
df.write.format("parquet").saveAsTable(permanent_table_name)
