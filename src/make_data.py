import pandas as pd
from pathlib import Path

# Global Variables
DATA_DIR = Path(__file__).resolve().parent.parent / "data"
RAW_DIR = DATA_DIR / "raw"
INTERIM_DIR = DATA_DIR / "interim"

# Loading in raw no show data
raw_data = pd.read_csv(RAW_DIR / "Medical_No_Shows.csv")
raw_data.columns = raw_data.columns.str.lower()

# -------------- Splitting data into different feature datasets -------------- #

# Demographics
patient = raw_data.loc[:, ["patientid", "gender", "age"]].drop_duplicates()

# patients that have multiple ages - just simplifying
keep_pts = (patient.patientid.value_counts()
            .reset_index()
            .query('patientid == 1')['index']
            .tolist())

# Removing patients with more than one age. Minimal rows
keep_pt = lambda x: x.query('patientid in @keep_pts')

# Saving to file
keep_pt(patient).to_parquet(INTERIM_DIR / "patient.parquet")

# Conditions
conditions = raw_data.loc[
    :,
    [
        "patientid",
        "medicaidind",
        "hypertension",
        "diabetes",
        "alcoholism",
        "disability",
    ],
].drop_duplicates()
keep_pt(conditions).to_parquet(INTERIM_DIR / "condition.parquet")

# Encounter
encounter = raw_data.loc[
    :,
    [
        "patientid",
        "appointmentid",
        "scheduledday",
        "appointmentday",
        "sms_received",
    ],
]
keep_pt(encounter).to_parquet(INTERIM_DIR / "encounter.parquet")

# Target
noshow = raw_data.loc[:, ["patientid", "appointmentid", "no-show"]]
keep_pt(noshow).to_parquet(INTERIM_DIR / "noshow.parquet")
